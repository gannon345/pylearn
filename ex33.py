def the_numbers(value, increment):
    i = 0
    numbers = []

    while i < value:
        print "At the top i is %d" % i
        numbers.append(i)

        i = i + increment
        print "Numbers now: ", numbers
        print "At the bottom i is %d" % i


    print "The numbers: "

    for num in numbers:
        print num
    return

while True:
    print "Enter the values below, or \'q\' to quit"
    x = raw_input("Enter a number to count up to: ")
    y = raw_input("Enter a number to increment the count by: ")

    if x is not 'q' or y is not 'q':
        the_numbers(int(x), int(y))
    else:
        break