print "Upon leaving the previous room, the door slams down behind you, leaving you trapped."
print "You wait for your eyes to adjust to the dim, straining your ears for any sound."
print "Hearing nothing, you sit down to rest."
print "You begin to feel tired, your eyes drooping as you begin to nod off."
print "Sleeping here in this dangerous place can't be a good idea, \nbut you are utterly exhausted."
while True:
    action = raw_input("\nRest now? Y/N? >> ")
    if action.lower() == 'y':
        print "\nRest now? Y/N? >> %s\n" % action
        print "You choose to rest for a bit. You close your eyes and sleep."
        print "\nAfter a time you awaken, sensing a brightening of the ambience."
        print "As you open your eyes you are stunned by what you see before you."
        print "The previously dim room is now brightly lit and filled with treasure!\n"
        print "Unable to believe your luck, you begin to fill your pockets with all"
        print "the gold coins and gems they can hold.\n"
        print "After taking your fill, you begin walking toward the now well lit exit,"
        print "However, as you do so, something glittering brightly catches your eye."
        print "\nNear the back wall of the room stands a pedestal with a gigantic,"
        print "brightly glowing blue diamond. It must be worth an incredible fortune,"
        print "more than everything else you've gathered combined."
        print "You ponder this for a moment. The exit is right behind the pedestal."
        action = raw_input("Do you leave with your wealth, or do you take the gem before you go? >> ")
    if not('take' and 'gem')and('leave' or 'go') in action.lower():
        print "Do you leave with your wealth, or do you take the gem before you go? >> %s \n\n" % action
        print "You think twice about taking the gem, but it seems too obvious a trap."
        print "With everything else you've encountered here, it seems too risky."
        print "You exit behind the pedestal, following a long corridor into the action room.\n\n"
            #goblet_room()
    elif 'take' and 'gem' in action.lower():
                #clear()
        print "Do you leave with your wealth, or do you take the gem before you go? >> %s\n\n" % action
        print "You greedily snatch up the giant gem and bolt for the door."
        print "Too little, too late it would seem."
                #dead("The door slams shut before you and the ceiling begins to slowly lower.\n"
                 #     "You kneel down as low as you can until you're prone on the floor."
                  #   "You close your eyes, accepting your fate as the ceiling comes down to\n"
                   #  "crush the life from you, and destroy everything in the room.")

    elif action.lower() == 'n':
            #clear()
        print "\nRest now? Y/N? >> %s\n" % action
        print "You decide not to rest. This doesn't seem like a place you'd ever want to sleep."
        print "Dangers abound, and you feel you'd best be on your way."
        print "Carefully crawling across the dark room, you find your way to the opposite wall."
        print "Feeling around in the darkness, you find the wall ends in an opening ahead."
        print "You walk onward."

#dark_room()