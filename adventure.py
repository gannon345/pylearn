# This is a test text adventure game, made for LPtHW exercise 45. I think it may have gotten a little bigger
# than intended, but the process was fun, and it only took a few days. Feel free to borrow it, manipulate or
# change or do whatever you want with it for the sake of learning, so long as its only for personal use.


from sys import exit

import os


# Clears the screen. Uses 'cls' for Windows, 'clear' for *nix systems.
def clear():
    os.system('cls' if os.name == 'nt' else 'clear')


# Called when you take a bad path and die. Takes in text string as argument and appends the statement below
# Then prompts to start over or quit.
def dead(why):
    print why, "\n\nSucks to be you. You're dead."
    new_try = raw_input("\n\nTry again? Y/N?\n> ")
    if new_try.lower() == 'y':
        start()
    else:
        exit(0)


# Called when invalid text is inputted.
def dumbass():
    print "What's wrong with you? Can't you see this place is dangerous? \nGet serious and type in something valid!\n"


# First room, starts the game.
def start():
    clear()
    while True:
        print '''
You find yourself in a dark cavern.
The air is damp and musty.
In the gloom, you see a single path to the north.
'''
        action = raw_input(">> ")

        if "north" in action.lower():
            clear()
            print ">> %s" % action
            lever_room()
        else:
            dumbass()


# Dead end room. Options to look at ledge, not look at ledge, and go back.
# Dead option if you don't look at the ledge and don't go back. The narrator is a spiteful bastard.
def dead_end():
    print "You reach a room with a seemingly bottomless cavern. " \
          "\nAt the edge of the cavernous maw you think you see something glittering."
    action = raw_input("Do you want to take a closer look? Y/N? \n> ")
    if action.lower() == 'y':
        dead("You realize it was just a trick of the eye as the ground crumbles beneath your"
             "\nfeet. Shame, that. You wonder how long it will be before you hit the bottom.")
    else:
        print "Glad to see you're not some sort of greedy buffoon. That was probably a trap."
        action = raw_input("You should probably go back. Shall we? Y/N? \n\n> ")
        if action.lower() == 'y':
            lever_room()
        elif action.lower() == 'n':
            dead("Fine then, stay here. See if I care. \nI'm not feeling creative right now.\n"
                 "I guess a boulder falls on you and you die. There. Idiot.")
        else:
            dumbass()


# Room with three levers. Options to look at them or to pull them.
def lever_room():
    print '''
You are in a dimly lit room with three levers.
There is a lever on the left wall, one on the middle wall ahead,
and another on the wall to your right.

They may require closer inspection.

What are you waiting for? Take a look already...
'''
    action = raw_input(">> ")
    clear()
    print ">> %r" % action

    if "look" in action.lower():
        if "look" and "left" in action.lower():
            print "\nThe leftmost lever looks nondescript, and is slightly rusted. " \
                  "The wall behind it is smooth to the touch.\n"
            lever_room()

        elif "look" and "middle" in action.lower():
            print "\nThe middle lever is ornate. It's odd, as the wall behind it is coarse\n " \
                  "and doesn't appear as though it would lead anywhere.\n"
            lever_room()
        elif "look" and "right" in action.lower():
            print "\nThere's a sign scrawled action to this lever that says 'safe route ahead.'\n" \
                  "Seems a bit dubious if you ask me.\n"
            lever_room()
        elif "look" and not("left" or "right" or "middle") in action.lower():
            print "\nWhich one do you want to look at?\n"
            lever_room()
        else:
            dumbass()
            lever_room()

    elif "pull" or "use" in action:

        if "left" in action:
            print "You pulled the left lever and an opening suddenly appears before you to the west. \nYou enter."
            dead_end()

        elif "middle" in action:
            print "\nYou pull the ornate looking lever on the wall directly before you."
            dead("\nSuddenly you feel an innate sense of dread. "
                 "This sense is vindicated as you \nare suddenly eviscerated by razors"
                 " that spring out from the wall.\n\n")

        elif "right" in action:
            print "You pull the rightmost lever. Yeah, the one with the dubious claim of safety."
            print "A doorway appears before you as if by magic. It\'s just kind of there now."
            action = raw_input("\nDo you want to step through? Y/N?\n>> ")

            if action.lower() == 'y':
                print "\nHey, it's your funeral, by all means, proceed.\n\n"
                snake_room()
            else:
                print "\nOh quit your whining. You're going through whether you like it or not."
                snake_room()

        else:
            dumbass()
            lever_room()


# Serpent room. Need to get past the giant snake without making noise.
# Options include 'sneak' 'stealth' 'creep' and 'crawl' and anything else gets you killed.
def snake_room():
    print '''
The room you are in now is lit dimly by a torch on the wall at the
end of the corridor. In the small, dim light, you see something that
looks suspiciously like scales. Very, very large scales you think.
This can't be good.
'''
    action = raw_input("Stealth seems to be in order. What to do? \n\n>> ")
    if "sneak" or "stealth" or 'creep' or 'crawl' or 'quiet' in action.lower():
        clear()
        print '''
You traverse the corridor quietly, careful not to make a sound.
As you reach the end, you're extra careful, and tiptoe through the door ahead.
You quietly close the door behind you, heaving a sigh of relief.
'''
        god_room()

    else:
        clear()
        dead("You aren't about all that sneaking. It just seems cowardly to you.\n"
             "You puff out your chest and spew out a mighty roar,\n"
             "sure that the snake or whatever it is will be truly frightened\n and awed by your courage."
             "\nIt isn\'t. 'No awe here' you think, as the reptile devours you whole.")


# Fuck this god. He's really a nobody from Boise with a cool spear.
# Seriously though, I wonder if utilizing a while loop when it prompts to kneel might be better than
# calling dumbass() and then calling god_room() again. It works, but I don't know if it's elegant.
def god_room():
    print '''
As the door clicks shut, you turn and are briefly blinded by a bright flash of light.
Shielding your eyes with your hand, you attempt to survey your surroundings.
'HALT MORTAL!!!' A loud voice bellows in the blinding emptiness.
I AM YOUR GOD!!! KNEEL BEFORE ME!!!' shouts the voice.
'DISOBEY AT YOUR PERIL. I AM NOT A KIND GOD, AND SHALL SMITE THEE.'


            'KNEEL! NOW!'
'''
    action = raw_input("\nDo you obey and kneel, or do you defy the being before you? \n\nKneel, Y/N? >> ")

    clear()

    if action.lower() == 'y':
        print "Kneel, Y/N? >> %s\n\n" % action
        print '''
You kneel before your new god. He beckons you come closer. You approach.
The being before you is golden, and vaguely man shaped.He radiates a bright,
golden light. You smile, secure in your belief in the good of this golden being.
'''
        dead("\nSuddenly, the 'god' pulls a golden spear of light seemingly from nowhere."
             "\nHe impales you with it and you disintegrate into nothingness."
             "\n 'YOU POOR FOOL.' he mutters. 'BUT ALL GODS REQUIRE SACRIFICES IN THE END.'"
             "Guess he was serious about that whole smiting thing after all...")
    elif action.lower() == 'n':
        print "Kneel, Y/N? >> %s\n" % action
        print '''
The \"god\'s\" face contorts with anger. A golden spear of light forms in his hand.
The being swiftly approaches approaches, and a hum fills the air. You hear a
loud 'CRACK!'

The man before you falls to the floor, dead. You approach and inspect the spear.

It's a bit singed, and there's a small inscription near the base."
You squint to read the tiny text. 'Made in Taiwan.'Huh. That was unexpected.

You shrug, and look around. Seeing only one exit,
you walk through the opposing doorway, into the next room.
'''
        dark_room()
    else:
        dumbass()
        god_room()


# This room is a critical junction to reach one of two decidedly similar and short endings.
# Depending on whether you choose to rest or move on, a global flag is set to alter the ending.
# Wondering if there's a better way to set treasure_flag for the ending. I couldn't think of a
# way to do this without making it a global.
def dark_room():
    print '''
Upon leaving the previous room, the door slams down behind you, leaving you
trapped. You wait for your eyes to adjust to the dim, straining your ears for
any sound. Hearing nothing, you sit down to rest. You begin to feel tired, your
eyes drooping as you begin to nod off. Sleeping here in this dangerous place
can't be a good idea, but you are utterly exhausted."
'''
    while True:
        action = raw_input("\nRest now? Y/N? >> ")
        if action.lower() == 'y':
            global treasure_flag
            treasure_flag = True
            clear()
            print "\nRest now? Y/N? >> %s\n" % action
            print '''
You choose to rest for a bit. You close your eyes and sleep.
After a time you awaken, sensing a brightening of the ambience.
As you open your eyes you are stunned by what you see before you.
The previously dim room is now brightly lit and filled with treasure!

Unable to believe your luck, you begin to fill your pockets with all
the gold coins and gems they can hold.

After taking your fill, you begin walking toward the now well lit exit,
However, as you do so, something glittering brightly catches your eye.

Near the back wall of the room stands a pedestal with a gigantic,
brightly glowing blue diamond. It must be worth an incredible fortune,
more than everything else you've gathered combined.
You ponder this for a moment. The exit is right behind the pedestal.


'''
            action = raw_input("Do you leave with your wealth, or do you take the gem before you go? \n>> ")
            if ('leave' or 'go')and not('take' and 'gem') in action.lower():
                clear()
                print "Do you leave with your wealth, or do you take the gem before you go? >> %s \n\n" % action
                print '''
You think twice about taking the gem, but it seems too obvious a trap.
With everything else you've encountered here, it seems too risky.
You exit behind the pedestal, following a long corridor into the next room.


'''
                goblet_room()
            elif 'take' and 'gem' in action.lower():
                clear()
                print "Do you leave with your wealth, or do you take the gem before you go? >> %s\n\n" % action
                print "You greedily snatch up the giant gem and bolt for the door.\n" \
                      "Too little, too late it would seem."
                dead("The door slams shut before you and the ceiling begins to slowly lower.\n"
                     "You kneel down as low as you can until you're prone on the floor."
                     "You close your eyes, accepting your fate as the ceiling comes down to\n"
                     "crush the life from you, and destroy everything in the room.")

        elif action.lower() == 'n':
            global treasure_flag
            treasure_flag = False
            clear()
            print "\nRest now? Y/N? >> %s\n" % action
            print '''
You decide not to rest. This doesn't seem like a place you'd ever want to sleep.
Dangers abound, and you feel you'd best be on your way. Carefully crawling
across the dark room, you find your way to the opposite wall. Feeling around in
the darkness, you find the wall ends in an opening ahead.

You walk onward.

'''
            goblet_room()

        else:
            dumbass()


# The final test. Only one of the goblets doesn't kill you when drinking from it...but then
# you have to drink from a second one... the odds aren't in your favor.
# I do wonder if maybe the goblet choice could be called as a function, but I don't know
# if it's worth the trouble to do so, since the second time it needs to be altered with one
# choice removed. I could probably call it as a list and pop the one that doesn't initially
# kill you...but as I said, dunno if it's worth the effort. It's only two lines to print...
def goblet_room():
    print '''
At the end of the corridor you find yourself in a small, bare room with a small table.
On the table are three numbered goblets and a scrawled sign.

The first goblet is filled with a viscous, bubbling blue fluid.

The second has a red mixture with swirling motes.

The third is clear, and appears to be simply water.
'''
    action = raw_input("Read the sign? Y/N? >> ")
    if action.lower() == 'y':
        clear()
        print '''
You read the sign.


'That which bubbles does not boil'
'Red is death without a foil.'
'Clear as crystal is not what it seems.'
'Drink to your health, or your death if you please.'

How ominous. And vague. Well, bottoms up.
'''
    else:
        print "You opt to just go with your gut. I have no idea why.\n" \
              "Well, good luck, I guess. You'll need it.\n"

    while True:
        action = raw_input("Do you drink from: \n1. The blue bubbling liquid?"
                           "\n2. The red mixture?"
                           "\n3. The clear fluid?\n>> ")
        if '1' in action or 'blue' in action:
            print "\nYou pinch your nose, reach for the first goblet and gulp it down." \
                  "\nNothing happens. Try drinking another one. Two goblets remain."
            action = raw_input("Do you drink from: \n2. The red mixture?"
                               "\n3. The clear fluid?\n>> ")

            if '2' in action or 'red' in action:
                clear()
                print "Do you drink from: \n2. The red mixture?\n3. The clear fluid?\n>> %s\n" % action
                print '''
There's something suspicious about that 'water' you think."
It's just too plain. You take the goblet of red fluid"
and swallow it down."

A section of the wall gives way across from the table.
The path before you is open. You heave a sigh of relief.
It looks like an exit. You smell fresh air. Finally.
'''
                exit_room()

            elif '3' in action or 'clear' in action:
                print '''
Alright, something was supposed to happen.
When you drink some blue bubbling sauce, you expect...
well, something, at least. Well, two goblets left.
Nervously, you drink the clear fluid."
'''
                dead("You quickly dissolve into a puddle of blue-ish goo."
                     "\nThat was quite the rapid chemical reaction.\n"
                     "Well, this just sucks, doesn't it.")

        elif '2' in action or 'red' in action:
            dead("You gulp down the glowing red mixture. You immediately regret it.\n"
                 "Your guts churn and you collapse to the floor, breathing rapidly.\n"
                 "Your vision fades as the poison takes hold, and you slowly die\n"
                 "in horrible gastric agony.")

        elif '3' in action or 'clear' in action:
            print '''
You figure the clear fluid has to be the safest bet.
It may just be water after all. It doesn't even have a smell.

Swallowing deeply, you brace for something horrible.
After a moment, nothing happens and you sigh with relief.
'''
            dead("Then you explode, leaving chunks of gore all over the room.")

        else:
            print "That's not a valid choice. \nSorry, you have to drink one of them.\n"


# Holy shit, the end! If you saw the treasure room (and lived), global treasure_flag == True
# This alters the ending slightly.
def exit_room():
    print '''
You can't believe it. You actually survived that heinous gauntlet.
You swear to yourself you are never, ever doing this again.
No more dark caverns and mythical quests for you, no sir.

'''
    if treasure_flag is True:
        print '''
...but on the bright side, with all the gold and gems
lining your pockets, you'll never need to.
You're filthy, stinking rich now. Woohoo!


THE END
'''
        exit(0)

    else:
        print "Time to head home...\n\nTHE END"
        exit(0)


# Clear the terminal/command prompt, then call start() to begin.
clear()
start()