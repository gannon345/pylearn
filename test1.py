from sys import exit

import os

os.system('cls' if os.name == 'nt' else 'clear')


def dead(why):
    print why, "Sucks to be you. You're dead."
    new_try = raw_input("\n\nTry again? Y/N?\n> ")
    if new_try == 'y' or 'Y':
        start()
    else:
        exit(0)


def dumbass():
    print "What's wrong with you? Can't you see this place is dangerous? Get serious!\n"


def start():
    os.system('cls' if os.name == 'nt' else 'clear')
    while True:
        print "You find yourself in a dark cavern."
        print "The air is damp and musty."
        print "In the gloom, you see a single path to the north."

        next = raw_input(">> ")
        print ""

        if "north" in next:
            os.system('cls' if os.name == 'nt' else 'clear')
            lever_room()
        else:
            dumbass()


def dead_end():
    print "You reach a room with a seemingly bottomless cavern. " \
          "At the edge of the cavernous maw you think you see something glittering."
    next = raw_input("Do you want to take a closer look? Y/N? \n> ")
    if next == 'y' or next == 'Y':
        dead("You realize it was just a trick of the eye as the ground crumbles beneath your"
             "feet. Shame, that. You wonder how long it will be before you hit the bottom.")
    else:
        print "Glad to see you're not some sort of greedy buffoon. That was probably a trap."
        next = raw_input("You should probably go back. Shall we? Y/N? \n\n> ")
        if next == 'y' or next == 'Y':
            lever_room()
        else:
            dead("Fine then, stay here. See if I care. \nI'm not feeling creative right now.\n"
                 "I guess a boulder falls on you and you die. There. Idiot.")


def lever_room():
    print "\nYou are now in a dimly lit room with three levers. "
    print "There is a lever on the left wall, one on the middle wall ahead, \nand another on the wall to your right.\n"
    print "They may require closer inspection."
    print "What are you waiting for? Take a look already..."

    next = raw_input(">> ")
    os.system('cls' if os.name == 'nt' else 'clear')
    print ">> %r" % next

    if "look" in next:
        if "look" and "left" in next:
            print "\nThe leftmost lever looks nondescript, and is slightly rusted. " \
                  "The wall behind it is smooth to the touch.\n"
            lever_room()

        elif "look" and "middle" in next:
            print "\nThe middle lever is ornate. It's odd, as the wall behind it is coarse\n " \
                  "and doesn't appear as though it would lead anywhere.\n"
            lever_room()
        elif "look" and "right" in next:
            print "\nThere's a sign scrawled next to this lever that says 'safe route ahead.'\n" \
                  "Seems a bit dubious if you ask me.\n"
            lever_room()
        else:
            dumbass()
            lever_room()

    elif "pull" or "use" in next:

        if "left" in next:
            print "You pulled the left lever and an opening suddenly appears before you to the west. \nYou enter."
            dead_end()

        elif "middle" in next:
            print "\nYou pull the ornate looking lever on the wall directly before you."
            dead("\nSuddenly you feel an innate sense of dread. "
                 "This sense is vindicated as you \nare suddenly eviscerated by razors"
                 " that spring out from the wall.\n\n")

        elif "right" in next:
            print "You pull the rightmost lever. Yeah, the one with the dubious claim of safety."
            print "A doorway appears before you as if by magic. It\'s just kind of there now."
            next = raw_input("Do you want to step through? Y/N?\n>> ")

            if next == 'y' or next == 'Y':
                print "\nHey, it's your funeral, by all means, proceed.\n\n"
                snake_room()
            else:
                print "\nOh quit your whining. You're going through whether you like it or not."
                snake_room()


        else:
            dumbass()
            lever_room()


def snake_room():
    print "The room you are in now is lit dimly by a torch on the wall at the end\nof the corridor."
    print "In the small, dim light, you see something that looks suspiciously like scales."
    print "Very, very large scales you think. This can't be good."
    next = raw_input("Stealth seems to be in order. What to do? \n\n>> ")
    if "sneak" or "stealth" in next:
        os.system('cls' if os.name == 'nt' else 'clear')
        print "\nYou traverse the corridor quietly, careful not to make a sound."
        print "As you reach the end, you're extra careful, and tiptoe through the door ahead.\n"
        print "You quietly close the door behind you, heaving a sigh of relief."
        god_room()

    else:
        os.system('cls' if os.name == 'nt' else 'clear')
        dead("You aren't about all that sneaking. It just seems cowardly to you.\n"
             "You puff out your chest and spew out a mighty roar,\n"
             "sure that the snake or whatever it is will be truly frightened\n and awed by your courage."
             "\nIt isn\'t. 'No awe here' you think, as the reptile devours you whole.")


def god_room():
    print "As the door clicks shut, you turn and are briefly blinded by a bright flash of light."
    print "Shielding your eyes with your hand, you attempt to survey your surroundings.\n"
    print " 'HALT MORTAL!!!' A loud voice bellows in the blinding emptiness."
    print "\n\t'I AM YOUR GOD!!! KNEEL BEFORE ME!!!' shouts the voice."
    print "'DISOBEY AT YOUR PERIL. I AM NOT A KIND GOD, AND SHALL SMITE THEE.\N\N\N KNEEL! NOW!"
    next = raw_input("\nDo you obey and kneel, or do you defy the being before you? \n\nKneel, Y/N? >> ")

    os.system('cls' if os.name == 'nt' else 'clear')

    if next == 'y' or next == 'Y':
        print "Kneel, Y/N? >> %s\n\n" % next
        print "You kneel before your new god. He beckons you come closer. You approach."
        print "The being before you is golden, andgenerally man shaped.\nHe radiates a bright, golden light."
        print "You smile, secure in your belief in the good of this golden being."
        dead("\nSuddenly, the 'god' pulls a golden spear of light seemingly from nowhere."
             "\nHe impales you with it and you disintegrate into nothingness."
             "\n 'YOU POOR FOOL.' he mutters. 'BUT ALL GODS REQUIRE SACRIFICES IN THE END.'"
             "Guess he was serious about that whole smiting thing after all...")
    else:
        print "Kneel, Y/N? >> %s\n" % next
        print "The \"god\'s\" face contorts with anger. A golden spear of light forms in his hand."
        print "The being swiftly approaches approaches, and a hum fills the air. \n\nYou hear a loud 'CRACK!'"
        print "\nThe man before you falls to the floor, dead. You approach and inspect the spear."
        print "\n It's a bit singed, and there's a small inscription near the base."
        print "You squint to read the tiny text. 'Made in Taiwan.'"
        print "\nHuh. That was unexpected. You shrug, and look around."
        print "\nSeeing only one exit, you walk through the opposing doorway, into the next corridor."
        # dark_room()


# def dark_room():


start()