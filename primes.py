def prime_test(user_val_low, user_val_high):

    prime_val = ""
    for i in xrange(user_val_low, user_val_high):
        if i < 4:
            print i, "is prime"
        else:
            for x in xrange(2, i/2 + 2):
                if i % x == 0:
                    prime_val = ""
                    break

                else:
                    prime_val = "is prime"

            print i, prime_val
    return

if __name__ == "__main__":
    print "search for prime numbers between two values.\n"
    print "Press CTRL+Z / CTRL+D to quit\n\n"
    while True:
        try:
            user_val_low = raw_input("enter the starting (low) number to search from: ")
            user_val_high = raw_input("enter the ending (high) number to search up to: ")
            prime_test(int(user_val_low), int(user_val_high) + 1)
        except:
            print "End of program."
            break