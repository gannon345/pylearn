from sys import argv # import argv from sys

script, input_file = argv # accept command line arguments (python script, external file)

# accepts argument file, prints the file
def print_stuff(f):
    print f.read()



# seeks first byte in the file
def rewind(f):
    f.seek(0)


# prints the linecount value and reads the line in the file
def print_a_line(line_count, f):
    print line_count, f.readline()


# opens the file from commandline argument
current_file = open(input_file)

print "First let's print the whole file:\n"

print_stuff(current_file) # pass current_file to function

print "Now let's rewind, like a cassette tape!"

rewind(current_file) # pass current_file to function

print "Let's print three lines:\n"

# call print_a_line three times, passing the values to function to rcv output.
current_line = 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)
current_file.close()