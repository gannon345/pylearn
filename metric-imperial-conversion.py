choice = 0

while choice != 'exit':
    print "Please choose a value to convert"
    print "For inches to centimeters: 1"
    print "For centimeters to inches: 2"
    print "For pounds to kilos: 3"
    print "For kilos to pounds: 4"
    print "Type 'exit' to quit"
    choice = raw_input("Enter your choice: ")

    if choice == '1':
        value = float(raw_input(
            "Enter value to convert from inches to centimeters: "))
        print "%.2f inches is equal to %.2f centimeters\n \n" % (
            value, value * 2.54)


    elif choice == '2':
        value = float(raw_input(
            "Enter value to convert from centimeters to inches: "))
        print "%.2f centimeters is equal to %.2f inches\n \n" % (
            value, value / 2.54)


    elif choice == '3':
        value = float(raw_input(
            "Enter value to convert from pounds to kilos: "))
        print "%.2f lbs is equal to %.2f kilos\n \n" % (
            value, value * 0.453592)


    elif choice == '4':
        value = float(raw_input(
            "Enter value to convert from kilos to pounds: "))
        print "%.2f kilos is equal to %.2f pounds\n \n" % (
            value, value *2.20462)


    else:
        print"Invalid Input\n \n"

