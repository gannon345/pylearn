
class Room(object):
    def __init__(self, **kw):
        self.id = kw['id']
        self.desc = kw['desc']
        self.exits = kw['exits']

def main():
    rooms = [
            Room(id=0,
                exits={'n':2},
                desc='a room'
                ),
            Room(id=1,
                exits={'s',1},
                desc='another room',
                )
            ]
    print('look room 0: {}'.format(rooms[0].desc))
    print('look room 1: {}'.format(rooms[1].desc))