# This is a test text adventure game, made for LPtHW exercise 45 using Python 2.7.
# Feel free to borrow it, manipulate it, change it,  or do whatever you want
# with it for the sake of learning, so long as its only for personal use.

from sys import exit
import os
from random import randint
from time import sleep

global yes_text
global no_text
yes_text = ['y', 'yes', 'yeah', 'afirmative', 'yep']
no_text = ['n', 'no', 'nope', 'negative', 'nay']


def clear():
    os.system('cls' if os.name == 'nt' else 'clear')
    return


def bad_input():
    print "This input is invalid! Enter a valid input!"
    sleep(1.5)
    clear()
    sleep(0.5)
    return


def take_words(input_string, action_string):

    input_words = input_string.lower().split()
    #print input_words
    action_words = action_string.lower().split()
    #print action_words
    result_len = len(action_words)
    #print result_len
    counter_list = []
    counter = 0
    for i in range(len(action_words)):
        for word in input_words:
            if word == action_words[i] and word not in counter_list:
                counter += 1
                counter_list.append(word)

    if counter >= result_len:
        #print "It matches."
        return True
    else:
        #print "No match"
        return False


# Main Scene class, default room setup.
# Designed to be abstracted for sub-classed scene objects.
class Scene(object):

    def start(self):
        print "This scene is not yet configured. Subclass it and implement start()."
        exit(1)

    #def bad_input(this_scene):
    #    print "\nThis input is invalid."
    #    return this_scene


class GameEngine(object):

    def __init__(self, scene_map):
        #print "Engine __init__ has scene_map", scene_map
        self.scene_map = scene_map

    def play(self):
        #import ipdb; ipdb.set_trace()
        current_scene = self.scene_map.opening_scene()
        #print "Play's first scene", current_scene

        while True:
            print "\n--------"
            #ipdb.set_trace()  #<= Added this line
            next_scene_name = current_scene.start()
            #print "next scene", next_scene_name
            current_scene = self.scene_map.next_scene(next_scene_name)
            #print "map returns new scene", current_scene


class Death(Scene):

    quips = [
        "You really aren't cut out for this, are you?",
        "You died. How's the afterlife treating you?",
        "Have you considered trying safer profession?"
        "Maybe this adventuring stuff just isn't for you."
        "Dead, huh? How many times is this now?"
        "Don't feel bad. It's not like the game is fair."
    ]

    def start(self):
        sleep(0.5)
        print Death.quips[randint(0, len(self.quips)-1)]
        sleep(0.5)
        return 'try_again'


class TryAgain(Scene):

    def start(self):
        action = raw_input("\nTry again? Y/N: ")

        if action[0].lower() in yes_text:
            return 'entrance'
        else:
            exit(0)


#Implement individual room subclasses
class Entrance(Scene):

    def start(self):

        print '''
You find yourself in a dark cavern.
The air is damp and musty.
In the gloom, you see a single path to the north.
'''
        action = raw_input(">> ")

        if take_words(action, "go north"):
            clear()
            print ">> %s" % action
            return 'lever_room'

        elif take_words(action, "look"):
            clear()
            print ">> %s\n" % action
            print "There's not much to see here. There's a path to the north.\n" \
                  "Everything else here is shrouded in darkness."
            sleep(3)
            return 'entrance'

        else:
            bad_input()
            return 'entrance'


class LeverRoom(Scene):

    def start(self):
        print '''
You are in a dimly lit room with three levers.
There is a lever on the left wall, one on the middle wall ahead,
and another on the wall to your right. You can probably pull one
of those, but who knows what might happen.

They may require closer inspection. You should probably look at
the levers and see if you figure out what to do.

What are you waiting for? Take a look already...
'''
        action = raw_input(">> ").lower()
        clear()
        print ">> %r" % action

        if 'look' in action:

            if take_words(action, "look around"):
                print "Looking around does seem like a good idea.\n" \
                      "Try taking a closer look at the levers on the walls."
                sleep(3)
                return 'lever_room'

            elif take_words(action, "look left lever"):
                print "\nThe leftmost lever looks nondescript, and is slightly rusted. " \
                    "\nThe wall behind it is smooth to the touch.\n"
                sleep(2.5)
                return 'lever_room'

            elif take_words(action, "look middle lever"):
                print "\nThe middle lever is ornate. It's odd, as the wall behind it is coarse\n " \
                      "and doesn't appear as though it would lead anywhere.\n"
                sleep(2.5)
                return 'lever_room'

            elif take_words(action, "look right lever"):
                print "\nThere's a sign scrawled action to this lever that says 'safe route ahead.'\n" \
                      "Seems a bit dubious if you ask me.\n"
                sleep(2.5)
                return 'lever_room'

            elif take_words(action, "look up"):
                print "\nOoh, a stalactite! Can we focus now, please?"
                sleep(2)
                return 'lever_room'

            elif take_words(action, "look down"):
                print "\nYou're looking at a nondescript floor. There's nothing here."
                sleep(2)
                return 'lever_room'

            else:
                print "I don't know what you're trying to look at."
                sleep(2)
                return 'lever_room'

        elif ('pull' or 'use') in action:

            if take_words(action, "pull left lever") or take_words(action, "use left lever"):
                print "You pulled the left lever and an opening suddenly appears before you to the west. \nYou enter."
                sleep(2)
                return 'dead_end'

            elif take_words(action, "pull middle lever") or take_words(action, "use middle lever"):
                print '''You pull the ornate looking lever on the wall directly before you.
Suddenly you feel an innate sense of dread.
This sense is vindicated as you are suddenly
eviscerated by razors that spring out from the wall.
'''
                sleep(2)
                return 'death'

            elif take_words(action, "pull right lever") or take_words(action, "use right lever"):
                print "You pull the rightmost lever. Yeah, the one with the dubious claim of safety" \
                      "\nYou're either really brave, or really stupid."
                sleep(2)
                print "Luckily for you, a doorway appears before you as if by magic." \
                      "\nIt\'s just kind of there now."
                sleep(.5)
                action = raw_input("\nDo you want to step through? Y/N?\n>> ")

                if action.lower() in yes_text:
                    print "\nHey, it's your funeral, by all means, proceed.\n\n"
                    sleep(2.5)
                    return 'snake_room'
                elif action.lower() in no_text:
                    print "\nOh quit your sniveling. You're going through whether you like it or not."
                    sleep(2.5)
                    return 'snake_room'
                else:
                    print "I don't even know what you're trying to do. Just stop sniveling" \
                          "\nand get going. Onward brave (*AHEM*) adventurer!"
                    sleep(2.5)
                    return 'snake_room'

            else:
                print "I don't even want to know what you're tugging on right now..."
                sleep(2.5)
                return 'lever_room'

        else:
                bad_input()
                return 'lever_room'


class DeadEnd(Scene):

    def start(self):
        print "You reach a room with a seemingly bottomless cavern. " \
              "\nAt the edge of the cavernous maw you think you see something glittering."
        action = raw_input("Do you want to take a closer look? Y/N? \n> ")
        if action.lower() in yes_text:
            print "You realize it was just a trick of the eye as the ground crumbles beneath your" \
                  "\nfeet. Shame, that. You wonder how long it will be before you hit the bottom."
            return 'death'

        else:
            print "Glad to see you're not some sort of greedy buffoon. That was probably a trap."
            action = raw_input("You should probably go back. Shall we? Y/N? \n\n> ")
            if action.lower() in yes_text:
                return 'lever_room'
            elif action.lower() in no_text:
                print "Fine then, stay here. See if I care. \nI'm not feeling creative right now.\n" \
                      "I guess a boulder falls on you and you die. There. Idiot."
                return 'death'

            else:
                bad_input()
                return 'dead_end'


class SnakeRoom(Scene):

    def start(self):
        print '''
The room you are in now is lit dimly by a torch on the wall at the
end of the corridor. In the small, dim light, you see something that
looks suspiciously like scales. Very, very large scales you think.
This can't be good.
'''
        action = raw_input("Stealth seems to be in order. What to do? \n\n>> ")
        if take_words(action, "look"):
            clear()
            print "Stealth seems to be in order. What to do? \n\n>> %s" % action
            sleep(0.5)
            print "The room is dimly lit. You see glittering scales.\n" \
                  "They appear to belong to some giant serpent.\n" \
                  "There is a door at the other end of the corridor."
            return 'snake_room'

        elif (take_words(action, "sneak past") or take_words(action, "stealth past") or
                take_words(action, "sneak by") or take_words(action, "stealth by")):
            clear()
            print '''
You traverse the corridor quietly, careful not to make a sound.
As you reach the end, you're extra careful, and tiptoe through the door ahead.
You quietly close the door behind you, heaving a sigh of relief.
'''
            sleep(2.5)
            return 'god_room'

        elif (take_words(action, "run")) or take_words(action, "fight"):
            clear()
            print "Stealth seems to be in order. What to do? \n\n>> %s" % action
            print '''
You aren't about all that sneaking. It just seems cowardly to you.
You puff out your chest and spew out a mighty roar, prepared to
 run or fight, sure that the giant snake or whatever it is will be
 truly frightened and awed by your courage.

It isn't. 'No awe here' you think, as the reptile darts out faster
than your eyes can even perceive and devours you whole.
'''
            sleep(2.5)
            return 'death'

        else:
            bad_input()
            sleep(2)
            return 'snake_room'


class GodRoom(Scene):

    def start(self):
        print '''
As the door clicks shut, you turn and are briefly blinded by a bright flash of light.
Shielding your eyes with your hand, you attempt to survey your surroundings.
'HALT MORTAL!!!' A loud voice bellows in the blinding emptiness.
I AM YOUR GOD!!! KNEEL BEFORE ME!!!' shouts the voice.
'DISOBEY AT YOUR PERIL. I AM NOT A KIND GOD, AND SHALL SMITE THEE.'


            'KNEEL! NOW!'
'''
        action = raw_input("\nDo you obey and kneel, or do you defy the being before you? \n\nKneel, Y/N? >> ")
        clear()

        if action.lower() in yes_text:
            print "Kneel, Y/N? >> %s\n\n" % action
            print '''
You kneel before your new god. He beckons you come closer. You approach.
The being before you is golden, and vaguely man shaped.He radiates a bright,
golden light. You smile, secure in your belief in the good of this golden being.
'''
            sleep(2)
            print'''
Suddenly, the 'god' pulls a golden spear of light seemingly from nowhere.
He impales you with it and you disintegrate into nothingness.
'YOU POOR FOOL.' he mutters. 'BUT ALL GODS REQUIRE SACRIFICES IN THE END.'
Guess he was serious about that whole smiting thing after all...
'''
            sleep(3)
            return 'death'

        elif action.lower() in no_text:
            print "Kneel, Y/N? >> %s\n" % action
            print 'You choose not to kneel before the so called god.'
            sleep(2)
            print'''
The "god's" face contorts with anger. A golden spear of light forms in his hand.
The being swiftly approaches approaches, and a hum fills the air. You hear a
loud 'CRACK!'
'''
            sleep(1.5)
            print'The man before you falls to the floor, dead. ' \
                 'You approach and inspect the spear.'
            sleep(1.5)
            print'''

It's a bit singed, and there's a small inscription near the base.
You squint to read the tiny text. 'Made in Taiwan.'Huh. That was unexpected.

You shrug, and look around. Seeing only one exit,
you walk through the opposing doorway, into the next room.
'''
            sleep(5)
            return 'dark_room'
        else:
            bad_input()
            return 'god_room'


class DarkRoom(Scene):

    def start(self):
        global treasure_flag
        print '''

Upon leaving the previous room, the door slams down behind you, leaving you
trapped. You wait for your eyes to adjust to the dim, straining your ears for
any sound. Hearing nothing, you sit down to rest. You begin to feel tired, your
eyes drooping as you begin to nod off. Sleeping here in this dangerous place
can't be a good idea, but you are utterly exhausted."
'''
        action = raw_input("\nRest now? Y/N? >> ")
        if action.lower() in yes_text:
            treasure_flag = True

            clear()
            print "\nRest now? Y/N? >> %s\n" % action
            print '''
You choose to rest for a bit. You close your eyes and sleep.
After a time you awaken, sensing a brightening of the ambience.
As you open your eyes you are stunned by what you see before you.
The previously dim room is now brightly lit and filled with treasure!

Unable to believe your luck, you begin to fill your pockets with all
the gold coins and gems they can hold.
'''
            sleep(3)
            print'''
After taking your fill, you begin walking toward the now well lit exit,
However, as you do so, something glittering brightly catches your eye.

Near the back wall of the room stands a pedestal with a gigantic,
brightly glowing blue diamond. It must be worth an incredible fortune,
more than everything else you've gathered combined.
You ponder this for a moment. The exit is right behind the pedestal.


'''
            sleep(3)
            while True:
                action = raw_input("Do you leave with your wealth, or do you take the gem before you go? \n>> ")
                if (take_words(action, "leave") or take_words(action, "go")) and not (
                        take_words(action, "with gem") or take_words(action, "take gem")):

                    clear()
                    print "Do you leave with your wealth, or do you take the gem before you go? >> %s \n\n" % action
                    print '''
You think twice about taking the gem, but it seems too obvious a trap.
With everything else you've encountered here, it seems too risky.
You exit behind the pedestal, following a long corridor into the next room.


'''
                    return 'goblet_room'

                elif take_words(action, "take gem") or take_words(action, "leave with gem"):
                    clear()
                    print "Do you leave with your wealth, or do you take the gem before you go? >> %s\n\n" % action
                    print "You greedily snatch up the giant gem and bolt for the door.\n" \
                          "Too little, too late it would seem."

                    print '''
The door slams shut before you and the ceiling begins to slowly lower.
You kneel down as low as you can until you're prone on the floor.
You close your eyes, accepting your fate as the ceiling comes down to
crush the life from you, and destroy everything in the room.
'''
                    return 'death'

                else:
                    bad_input()
                    sleep(3)
                    continue

        elif action.lower() in no_text:
            treasure_flag = False
            clear()
            print "\nRest now? Y/N? >> %s\n" % action
            print '''
You decide not to rest. This doesn't seem like a place you'd ever want to sleep.
Dangers abound, and you feel you'd best be on your way. Carefully crawling
across the dark room, you find your way to the opposite wall. Feeling around in
the darkness, you find the wall ends in an opening ahead.

You walk onward.

'''
            sleep(5)
            return 'goblet_room'

        else:
            bad_input()
            return 'dark_room'


class GobletRoom(Scene):

    def start(self):
        print '''
At the end of the corridor you find yourself in a small, bare room with a small table.
On the table are three numbered goblets and a scrawled sign.

The first goblet is filled with a viscous, bubbling blue fluid.

The second has a red mixture with swirling motes.

The third is clear, and appears to be simply water.
'''
        action = raw_input("Read the sign? Y/N? >> ")
        if action.lower() in yes_text:
            sleep(0.5)
            clear()
            print '''
You read the sign.
'''
            sleep(2)
            print'''

'That which bubbles does not boil'
'Red is death without a foil.'
'Clear as crystal is not what it seems.'
'Drink to your health, or your death if you please.'
'''
            sleep(5)
            print"How ominous. And vague. Well, bottoms up."

        else:
            clear()
            print "Read the sign? Y/N? >> %s" % action
            sleep(.5)
            print "You opt to just go with your gut. I have no idea why.\n" \
                  "Well, good luck, I guess. You'll need it.\n"

        action = raw_input("Do you drink from: \n\n"
                           "1. The blue bubbling liquid?"
                           "\n2. The red mixture?"
                           "\n3. The clear fluid?\n>> ")

        if ((take_words(action, "1") or take_words(action, "blue")) and not
                (take_words(action, "don't") or take_words(action, "do not"))):

            print "\nYou pinch your nose, reach for the first goblet and gulp it down." \
                  "\nNothing happens. Try drinking another one. Two goblets remain."

            while True:
                action = raw_input("Do you drink from: \n2. The red mixture?"
                                   "\n3. The clear fluid?\n>> ")

                if ((take_words(action, "2") or take_words(action, "red")) and not
                        (take_words(action, "don't") or take_words(action, "do not"))):
                    clear()
                    print "Do you drink from: \n2. The red mixture?\n3. The clear fluid?\n>> %s\n" % action
                    print '''
There's something suspicious about that 'water' you think."
It's just too plain. You take the goblet of red fluid"
and swallow it down."

A section of the wall gives way across from the table.
The path before you is open. You heave a sigh of relief.
It looks like an exit. You smell fresh air. Finally.
'''
                    return 'exit_room'

                elif ((take_words(action, "3") or take_words(action, "clear")) and not
                        (take_words(action, "don't") or take_words(action, "do not"))):
                    print '''
Alright, something was supposed to happen, you think.
When you drink some blue bubbling sauce, you expect...
well, something, at least. Well, two goblets left.
'''
                    sleep(2)
                    print "Nervously, you drink the clear fluid."
                    sleep(2)
                    print '''
You quickly dissolve into a puddle of blue-ish goo.
That was quite the rapid chemical reaction.
Well, this just sucks, doesn't it.
'''
                    return 'death'

                else:
                    bad_input()
                    sleep(2)

        elif ((take_words(action, "2") or take_words(action, "red")) and not
              (take_words(action, "don't") or take_words(action, "do not"))):
            print '''
You gulp down the glowing red mixture. You immediately regret it.
Your guts churn and you collapse to the floor, breathing rapidly.
Your vision fades as the poison takes hold, and you slowly die
in horrible gastric agony.
'''
            return 'death'

        elif ((take_words(action, "3") or take_words(action, "clear")) and not
              (take_words(action, "don't") or take_words(action, "do not"))):
            print '''
You figure the clear fluid has to be the safest bet.
It may just be water after all. It doesn't even have a smell.

Swallowing deeply, you brace for something horrible.
After a moment, nothing happens and you sigh with relief.

Then you explode, leaving chunks of gore all over the room.
'''
            return 'death'

        else:
            print "That's not a valid choice. \nSorry, you have to drink one of them.\n"
            sleep(2)
            clear()
            return 'goblet_room'


class ExitRoom(Scene):

    def start(self):
        print '''
You can't believe it. You actually survived that heinous gauntlet.
You swear to yourself you are never, ever doing this again.
No more dark caverns and mythical quests for you, no sir.

'''
        if treasure_flag is True:
            print '''
...but on the bright side, with all the gold and gems
lining your pockets, you'll never need to.
You're filthy, stinking rich now. Woohoo!


THE END
'''
            exit(0)

        else:
            print "Time to head home...\n\nTHE END"
            exit(0)


# handles scene calls to start specific scenes sub-classed from Scene()
# called via "SceneHandler(scene_name_from_SceneHandler_dict)
class SceneHandler(object):

    scene = {
        'entrance': Entrance(),
        'lever_room': LeverRoom(),
        'death': Death(),
        'try_again': TryAgain(),
        'dead_end': DeadEnd(),
        'snake_room': SnakeRoom(),
        'god_room': GodRoom(),
        'dark_room': DarkRoom(),
        'goblet_room': GobletRoom(),
        'exit_room': ExitRoom(),
    }

    def __init__(self, start_scene):
        self.start_scene = start_scene
        #print "start_scene in __init__", self.start_scene

    def next_scene(self, scene_name):
        #print "start_scene in next_scene"
        next_scene = SceneHandler.scene.get(scene_name)
        #print "next_scene returns", next_scene
        return next_scene

    def opening_scene(self):
        return self.next_scene(self.start_scene)


if __name__ == "__main__":

    a_scene = SceneHandler('entrance')
    a_game = GameEngine(a_scene)
    a_game.play()
