from sys import argv

script, filename = argv

print "we're going to erase %r/" % filename
print "If you don't want that , hit CTRL-C (^C)."
print "If you do want that, hit RETURN."

raw_input("?")

print "Opening the file..."
target = open(filename, 'r+')

print "Truncating the file. Goodbye!"
target.truncate()

print "Now I'm going to ask you for three lines."

line1 = raw_input("Line 1: ")
line2 = raw_input("Line 2: ")
line3 = raw_input("Line 3: ")

print "I'm going to write these to the file."

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

print"\n\nI'm going to read the file now, here's what you typed:\n\n"


print line1+'\n'+line2+'\n'+line3+'\n'

print "And finally, we close the file."
target.close()
