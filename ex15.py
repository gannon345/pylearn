# import argv to accept argument at command line
from sys import argv

# call argv at command line and set var 'filename' to the file typed at prompt
script, filename = argv

# opens 'filename'
txt = open(filename)

# prints, then calls the file name, then pastes the text to cmdline
print "Here's your file %r:" % filename
print txt.read()

# close the file
txt.close()


# prompts the user for the filename now, accepts raw input string. Can open any file
print "Type the filename again:"
file_again = raw_input("> ")

# opens the file. Calling it "file_again" isn't quite right, I can open any file here
txt_again = open(file_again)

# prints it to the cmdline
print txt_again.read()

# close the file
txt_again.close()