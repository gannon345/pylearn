from sys import exit
from random import randint


class Scene(object):
    pass

class Map(object):

    scenes = {
        'entrance': Entrance(),
        'lever_room': LeverRoom(),
        'death': Death()
    }

    def __init__(self, start_scene):
        self.start_scene = start_scene

    def next_scene(self scene_name):
        value = Map.scenes.get(scene_name)

    def opening_scene(self):
        return self.next_scene(self.start_scene)


class Death(Scene):

    def try_again(self):
        action = raw_input("Try again? Y/N:")
        print ">> "

        if action[0].lower() == 'y':
            return Entrance()
        else:
          exit(0)

    quips = [
        "You really aren't cut out for this, are you?",
        "You died. How's the afterlife treating you?",
        "Have you considered trying safer profession?"
        "Maybe this adventuring stuff just isn't for you."
        "Dead, huh? How many times is this now?"
        "Don't feel bad. It's not like the game is fair."
    ]

    def enter(self):
        print Death.quips[randint(0, len(self.quips)-1)]
        return Death.try_again




#Main Scene class, default room setup
class Scene(object):

    def enter(selfself):
        pass


#Implement individual room subclasses
class Entrance(Scene):

    def enter(self):
        pass


class LeverRoom(Scene):

    def enter(self):
        pass
