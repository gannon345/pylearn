from sys import exit
from random import randint


class Scene(object):

    def enter(self):
        print "This scene is not yet configured. Subclass it and implement enter()."
        exit(1)


class Engine(object):

    def __init__(self, scene_map):
        print "Engin __init__ has scene_map", scene_map
        self.scene_map = scene_map

    def play(self):
        current_scene = self.scene_map.opening_scene()
        print "Play's first scene", current_scene

        while True:
            print "\n------------"
            next_scene_name = current_scene.enter
            print "next_scene", next_scene_name
            current_scene = self.scene_map.next_scene(next_scene_name)
            print "map returns new scene", current_scene


class Death(Scene):

    quips = [
        "You died. You kind of suck at this.",
        "Your mom would be proud...if she were smarter.",
        "Such a loser...",
        "I have a small puppy that's better at this."
        "...you're really not cut out for this."
    ]

    def enter(self):
        print Death.quips[randint(0, len(self.quips)-1)]
        exit(1)


class CentralCorridor(Scene):

    def enter(self):
        print '''
The Gothons of planet Percal #25 have invaded your ship and destroyed
your entire cre. You are the last surviving member and your last
mission is to get the neutron destruct bomb from the weapons armory,
put it on the bridge, and blow the ship after getting to an escape pod

You're running down the central corridor to the weapons aromry when
a Gothon jumps out, red scaly skin, dark grimy teeth, and eveil clown costume
flowing around his hate filled body. He's blocking the door to the
armory and about to pull a weapon to blast you into space dust.
'''
        if action == "shoot!":
            print '''
Quick on the draw you yank out your blaster and fire it at the Gothon.
Your laser burns a hole in his flowing costume but misses him entirely.
This completely ruins his brand new costume and makes him fly into an
insane rage. He blasts you in the face repeatedly until your own mother
wouldn't recognize you. Then he eats you.'''
            return 'death'

        elif action == "dodge!":
            print '''
You duck and weave like a world class boxer, sliding to the right as the
Gothon's laser blasts past your head. Sadly, your foot catches on a
floor plate and you trip, bashing your head on the corridor's metal floor.
You wake up shortly thereafter to a Gothon stomping your face in.
Then he eats you.'''
            return 'death'

        elif action = "tell a joke":
            print '''
Luckily, they made you learn Gothon insults in the academy.
You tell the one Gothon joke you know:

Lbhe zbhgreu vf snrg, jura fur fvgf brff juehs huerer reff frrns nagauq.

The Gothon stops, chortles, and then lets out a full belly laugh. At
least you think that's it's belly.
While he's laughing you run up and shoot him square in the face, putting
him down, and then dash through the weapon armory door.'''
            return 'laser_weapon_armory'

        else:
            print "Does not compute!"
            return 'central_corridor'



class LaserWeaponArmory(Scene):

    def enter(self):
        pass


class TheBridge(Scene):

    def enter(self):
        pass


class EscapePod(Scene):

    def enter(self):
        pass


class Map(object):

    def __init__(self, start_scene):
        pass

    def next_scene(self, scene_name):
        pass

    def opening_scene(self):
        pass


a_map = Map('central_corridor')
a_game = Engine(a_map)
a_game.play