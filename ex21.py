def add(a, b):
    print "ADDING %d + %d" % (a, b)
    return a + b


def subtract(a, b):
    print "SUBTRACTING %d - %d" % (a, b)
    return a - b


def multiply(a, b):
    print "MULTIPLYING %d * %d" % (a, b)
    return a * b


def divide(a, b):
    print "DIVIDING %d / %d" % (a, b)
    return a / b


print "Let's do some math with just functions!"

age = add(30, 5)
height = subtract(78, 4)
weight = multiply(90, 2)
iq = divide(100, 2)

print "Age: %d, Height: %d, Weight: %d, IQ: %d" % (age, height, weight, iq)


# extra credit puzzle
print "Here is a puzzle."
what = add(age, subtract(height, multiply(weight, divide(iq, 2))))

print "That becomes: ", what, "Can you do it by hand?"
print "\n\n"

print "Now let's try some input from you."

while True:
    num1 = int(raw_input("Enter the first number: "))
    num2 = int(raw_input("Now enter the second number:"))

    choice = raw_input(" Would you like to (1) add, (2) subtract, (3) multiply, (4) divide, or (q) quit? ")

    if choice == '1':
        num_value = add(num1, num2)
        print num_value
    elif choice == '2':
        num_value = subtract(num1, num2)
        print num_value
    elif choice == '3':
        num_value = multiply(num1, num2)
        print num_value
    elif choice == '4':
        num_value = divide(num1, num2)
        print num_value
    elif choice == 'q' or 'Q':
        print "Quitting the program"
        break
    else:
        print "You didn't make a valid choice!\n"