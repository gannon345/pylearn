name = 'Jeff Gannon'
age = 31
height = 67 #inches
weight = 180 #lbs
eyes = 'Hazel'
teeth = 'White'
hair = 'Brown'

print "Let's talk about %s." % name
print "He's %d inches tall." % height
print "He's %d pounds heavy." % weight
print "Actually that's not too heavy."
print "He's got %s eyes and %s hair" % (eyes, hair)
print "His teeth are usually %s depending on the coffee" % teeth


print "If I add %d, %d, and %d I get %d." % (
    age, height, weight, age + height + weight)
